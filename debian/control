Source: eclipse-gef
Priority: optional
Section: devel
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Jakub Adam <jakub.adam@ktknet.cz>
Build-Depends: debhelper (>= 8~),
               default-jdk,
               eclipse-pde (>= 3.8.1-8),
               javahelper,
               unzip,
               zip
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-java/eclipse-gef.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/eclipse-gef.git
Homepage: http://www.eclipse.org/gef/

Package: eclipse-gef
Architecture: all
Depends: eclipse-platform (>= 3.8.1-8),
         ${misc:Depends}
Suggests: eclipse-gef-doc
Description: Eclipse Graphical Editing Framework
 The Graphical Editing Framework (GEF) provides technology to create rich
 graphical editors and views for the Eclipse Workbench UI. It bundles three
 components:
 .
  * Draw2d (org.eclipse.draw2d) - A layout and rendering toolkit for displaying
    graphics on an SWT Canvas.
  * GEF (MVC) (org.eclipse.gef) - An interactive model-view-controler (MVC)
    framework, which fosters the implementation of SWT-based tree and
    Draw2d-based graphical editors for the Eclipse Workbench UI.
  * Zest (org.eclipse.zest) - A visualization toolkit based on Draw2d, which
    enables implementation of graphical views for the Eclipse Workbench UI.

Package: eclipse-gef-doc
Section: doc
Architecture: all
Depends: eclipse-platform (>= 3.8.1-8),
         ${misc:Depends}
Suggests: eclipse-gef
Description: Eclipse Graphical Editing Framework (documentation)
 The Graphical Editing Framework (GEF) provides technology to create rich
 graphical editors and views for the Eclipse Workbench UI. It bundles three
 components:
 .
  * Draw2d (org.eclipse.draw2d) - A layout and rendering toolkit for displaying
    graphics on an SWT Canvas.
  * GEF (MVC) (org.eclipse.gef) - An interactive model-view-controler (MVC)
    framework, which fosters the implementation of SWT-based tree and
    Draw2d-based graphical editors for the Eclipse Workbench UI.
 * Zest (org.eclipse.zest) - A visualization toolkit based on Draw2d, which
   enables implementation of graphical views for the Eclipse Workbench UI.
 .
 Plugin documentation that can be viewed within Eclipse.
